var AdminScreen = window.AdminScreen || {};
var v_CutitronicsBrandID = "";

(function createPrescriptionScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });

    $(function onDocReady() {

        console.log("");
        console.log("create-question load function running...");
        console.log("");

        //Get parameters from URL
        var query = decodeURIComponent(window.location.search.substring(1));
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });

        //Set global variables
        v_CutitronicsBrandID = result.BrandID;

        //Get question set for Brand
        $.ajax({
            type: 'GET',
            url: window._config.api.invokeUrl + "/HAYGOQuestions/getBrandQuestionSet",
            headers: {
                Authorization: authToken
            },
            success: function fn_fillQSetSelect(result) {
                console.log("");
                console.log("Successfully got question sets from back office");
                console.log("Result:" + JSON.stringify(result));

                
                lv_QSetSelect = $('#QuestionSet');

                lv_QuestionSets = result.ServiceOutput.QuestionSets;

                console.log("Question Sets FROM API");
                console.log(lv_QuestionSets);
                console.log("");

                for(let lv_QuestionIndex in lv_QuestionSets){
                    
                    console.log("Question set with index");
                    console.log(lv_QuestionSets[lv_QuestionIndex]);
                
                    lv_SelectHTML = "<option value='" + lv_QuestionSets[lv_QuestionIndex] + "'> " + lv_QuestionSets[lv_QuestionIndex] + " </option>";

                    console.log("Select HTML" + lv_SelectHTML);

                    lv_QSetSelect.append(lv_SelectHTML);
                }

                //Show Select
                $('#QuestionSelect').fadeIn();
            },
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Response: ', jqXHR.responseText);
                alert('An error occured when calling the API:\n' + jqXHR.responseText);
            },
            data: {
                CutitronicsBrandID: v_CutitronicsBrandID
            }
        })


        //Add listener to question set select
        $('#QuestionSet').change(function (e) { 
            e.preventDefault();
            
            var lv_QSetName = $('#QuestionSet').val();
            
            if(lv_QSetName == "NewQSet"){
                $('#NewQSetBox').fadeIn();
            } else {
                $('#NewQSetBox').fadeOut();
            }
        });

        // Add listener to create question button 
        $("#createQuestion").click(function (e) { 
            e.preventDefault();

            if(v_CutitronicsBrandID != "") {

                var lv_Payload = {};
                console.log($('#QuestionSetName').val());
                console.log($('#QSetName').val());

                //Create payload based of textbox value
                if($('#QuestionSet').val() == "NewQSet"){

                    console.log("Creating New Question Set");
                    console.log($('#QuestionSetName').val());
                    //get new question from textbox
                    lv_Payload = {
                        "CutitronicsBrandID": v_CutitronicsBrandID,
                        "QuestionSet": $('#QuestionSetName').val(),
                        "QuestionText": $('#QuestionText').val()
                    }

                } else {
                    
                    console.log("Existing Question Set");
                    console.log($('QSetName').val());
                    //get new question from select
                    lv_Payload = {
                        "CutitronicsBrandID": v_CutitronicsBrandID,
                        "QuestionSet": $('#QuestionSet').val(),
                        "QuestionText": $('#QuestionText').val()
                    }
                }

                console.log("");
                console.log("Calling fn_createNewQuestion with the following payload");
                console.log("Payload: " + JSON.stringify(lv_Payload));

                fn_createNewQuestion(lv_Payload);

            }

            //if newQuestion not empty
            
        });


        // Add Listener to back button
        $("#BackBtn").click(function (e) { 
            window.history.back();
        });

    })

    //Fill Question Set Select
    function fn_fillQSetSelect(result){

        console.log("");
        console.log("Successfully got question sets from back office");
        console.log("Result:" + JSON.stringify(result));

        
        lv_QSetSelect = $('#QuestionSet');

        lv_QuestionSets = result.ServiceOutput.QuestionSets;

        console.log("Question Sets FROM API");
        console.log(lv_QuestionSets);
        console.log("");

        for(let lv_QuestionIndex in lv_QuestionSets){
            
            console.log("Question set with index");
            console.log(lv_QuestionSets[lv_QuestionIndex]);
        
            lv_SelectHTML = "<option value='" + lv_QuestionSets[lv_QuestionIndex] + "'> " + lv_QuestionSets[lv_QuestionIndex] + " </option>";

            console.log("Select HTML" + lv_SelectHTML);

            lv_QSetSelect.append(lv_SelectHTML);
        }

        //Show Select
        $('#QuestionSelect').fadeIn();

    }
    

    function fn_createNewQuestion(lv_Payload) {

        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + "/HAYGOQuestions/createQuestion",
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: fn_ShowCreateSuccess,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed API Request")
                console.error('Response: ', jqXHR.responseText);
            },
            data: JSON.stringify(lv_Payload)
        });

    }

    function fn_ShowCreateSuccess(result) {
        console.log("");
        console.log("Successfully created product: " + JSON.stringify(result));
        console.log("");

        window.alert("Successfully added new question '" + result.ServiceOutput.QuestionText + "'");
        window.location.href = '/HAYGO-Questions.html';

    }
    
}(jQuery));