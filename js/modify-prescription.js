var AdminScreen = window.AdminScreen || {};

(function addProductScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });


    function fn_DisplayPresDet(lv_PrescriptionDetails) {
        //Change placeholders in display usage form
        console.log("PresName: " + lv_PrescriptionDetails.PrescriptionName);
        console.log("PresID: " + lv_PrescriptionDetailsCutitronicsPresID);
        console.log("SkinType: " + lv_PrescriptionDetailsSkinType);

        //Change usage form placeholders
        $('#presName').attr('placeholder', lv_PrescriptionDetails.PrescriptionName);
        $('#PresID').attr('placeholder', lv_PrescriptionDetailsCutitronicsPresID);
        $('#skinType').attr('placeholder', lv_PrescriptionDetailsSkinType);
    }

    function fn_UpdatePresDetails(lv_UpdatePresInput) {
        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + '/prescriptions/updatePrescription',
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: fn_CompletePresUpdate,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed API Request: Update Prescription");
                console.error('Error updating prescription: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
            },
            data: JSON.stringify(lv_UpdatePresInput)
        });
    }

    function fn_CompletePresUpdate(result) {
        console.log("Completed API Request");
        console.log("Result" + result);
        window.location.href = "/prescription-products.html?"+ result.ServiceOutput.CutitronicsPresID + "|" + result.ServiceOutput.PrescriptionName + "|" + result.ServiceOutput.SkinType;
    }
    
    $(function onDocReady() {

        //Get Prescription Details - From URL?

        var query = decodeURIComponent(window.location.search.substring(1));
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });
        

        console.log("Prescription Name: " + result.PresName);
        console.log("Prescription ID: " + result.PresID);
        console.log("Skin Type: " + result.SkinType);

        //Display Prescription Details

        $('#PresID').html(result.PresID);
        $('#HiddenPresID').attr('value', result.PresID);
        
        $('#presName').attr('placeholder', result.PresName);
        $('#skinType').attr('placeholder', result.SkinType);
        
        //Prepare Data for ajax call

        lv_PrescriptionDetails = {
            "CutitronicsPresID": result.PresID,
            "PrescriptionName": result.PresName,
            "SkinType": result.SkinType
        }

        $('#UsageDetForm').submit(function (e) { 
            e.preventDefault();
            
            lv_UpdatePresInput = {
                "CutitronicsPresID": $('#HiddenPresID').val()
            }

            //Get form values if they have changed
            if($('#presName').val().trim().length === 0) {
                lv_UpdatePresInput.PrescriptionName = result.PresName;
            } else {
                lv_UpdatePresInput.PrescriptionName = $('#presName').val();
            }

            if($('#skinType').val().trim().length === 0) {
                lv_UpdatePresInput.SkinType = result.SkinType;
            } else {
                lv_UpdatePresInput.SkinType = $('#skinType').val();
            }

            fn_UpdatePresDetails(lv_UpdatePresInput);
        });
    })


  
}(jQuery));