var AdminScreen = window.AdminScreen || {};
var Globals = {};

(function getCutitronicsBrandsWrapper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });


    $(function onDocReady() {

        //Get Brand Details
        $.ajax({
            type: 'GET',
            url: window._config.api.getBrandsURL,
            headers: {
                Authorization: authToken
            },
            success: fn_DisplayBrands,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Error getting cutitronics brands: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
            }
        })
        
    })

    function fn_DisplayBrands(result){

        //Get cutitronics brands from result
        var lv_CutitronicsBrands = result.ServiceOutput.CutitronicsBrands

        var lv_BrandSelectObj = $('#CutitronicsBrandID')

        for(let lv_Brand of lv_CutitronicsBrands) {
            //FIXME Testing only
            console.log(lv_Brand)

            lv_BrandSelectHTML = "<option value='" + lv_Brand.CutitronicsBrandID + "'> " + lv_Brand.BrandName +"  </option>"
            lv_BrandSelectObj.append(lv_BrandSelectHTML);
        }
    }    

}(jQuery));