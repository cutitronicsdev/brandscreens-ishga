var AdminScreen = window.AdminScreen || {};

(function addProductScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in");
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });


    $(function onDocReady() {


        console.log("")
        console.log("Getting cutitronics brands")
        console.log("")
        
        $.ajax({
            type: 'GET',
            url: window._config.api.getBrandsURL,
            headers: {
                Authorization: authToken
            },
            success: fn_DisplayBrands,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Error getting cutitronics brands: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
            }
        })


    })


    //Completion handler for getCutitronicsBrand API Call
    function fn_DisplayBrands(result){

        //Get Cutitronics brands from result
        var lv_CutitronicsBrands = result.ServiceOutput.CutitronicsBrands

        var lv_BrandSelectObj = $('#CutitronicsBrandID')

        for(let lv_Brand of lv_CutitronicsBrands) {
            //FIXME Testing only
            console.log(lv_Brand)

            lv_BrandSelectHTML = "<option value='" + lv_Brand.CutitronicsBrandID + "'> " + lv_Brand.BrandName +"  </option>"
            lv_BrandSelectObj.append(lv_BrandSelectHTML);
        }
    }


    //Ajax call to add a new brand product
    function addBrandProduct(lv_BrandProduct) {
        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + '/products/createBrandProduct',
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: completeRequest,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Could not add new brand product '"+lv_BrandProduct.SKUName+"'");
            },
            data: JSON.stringify(lv_BrandProduct)
        });
    }

    function completeRequest(result) {
        window.alert("Sucessfully added product into back office systems");
        console.log('Response received from API: ', JSON.stringify(result));
        //Re-direct to view brand products page
        window.location.href = '/view-products.html';
    }


    $('#request').click(function (e) { 
        e.preventDefault();
        
        var lv_BrandProduct = {
            "BrandID": $('#CutitronicsBrandID').val(),
            "SKUCode": $('#SKUCode').val(),
            "SKUCat": $('#CutitronicsSKUCat').val(),
            "SKUType": $('#CutitronicsSKUType').val(),
            "SKUName": $('#CutitronicsSKUName').val(),
            "SKUDesc": $('#CutitronicsSKUDesc').val(),
            "DefaultURL": $('#DefaultURL').val(),
            "DefaultIMG": $('#DefaultIMG').val(),
            "DefaultVideo": $('#DefaultVideo').val(),
            "DefaultVolume": $('#DefaultVolume').val(),
            "DefaultParameters": $('#DefaultParameters').val(),
            "RecommendedAmount": $('#RecommendedAmount').val(),
            "Source": "BrandAdminScreen"
        }
        addBrandProduct(lv_BrandProduct);

    });

}(jQuery));