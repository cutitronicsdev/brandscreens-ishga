var AdminScreen = window.AdminScreen || {};
var Prescription = {};

(function prescriptionProductsScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });
    
    function getPrescribedProducts(lv_CutitronicsPresID){
        console.log("Prescription ID: " + lv_CutitronicsPresID);

        $.ajax({
            type: 'GET',
            url: window._config.api.invokeUrl + '/prescriptions/getPrescriptionProducts',
            headers: {
                Authorization: authToken
            },
            success: completeRequest,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.log("Could not find any prescribed products");

                var lv_ServiceOutput = jqXHR.responseText.ServiceOutput;

                //Get prescription details from response
                var lv_CutitronicsBrandID = lv_ServiceOutput.CutitronicsBrandID;
                var lv_CutitronicsPresID = lv_ServiceOutput.CutitronicsPresID;
                var lv_PrescriptionName = lv_ServiceOutput.PrescriptionName;
                var lv_SkinType = lv_ServiceOutput.SkinType;
                
                console.log(jqXHR.responseText);


                //Add product button
                var lv_ProductButton = "<a class='btn btn-outline-light' href='/add-pres-product.html?BrandID=" + lv_CutitronicsBrandID + "&PresID=" + lv_CutitronicsPresID + "'>Add Product</a> <a class='btn btn-outline-light pl-2' href='/modify-prescription.html?PresID=" + lv_CutitronicsPresID + "&PresName=" + lv_PrescriptionName + "&SkinType=" + lv_SkinType + "'>Modify Prescription Details</a>";
                $('#addProduct').html(lv_ProductButton);


                //Display no products message
            },
            data: {
                CutitronicsPresID: lv_CutitronicsPresID
            }
        })
    }


    function completeRequest(result){

        console.log("Result: ");
        console.log(result);
        console.log("ServiceOutput: ");
        console.log(result.ServiceOutput.PresProducts);

        //Get Pres Products and Cache Product Div
        lv_PrescriptionProducts = result.ServiceOutput.PresProducts;

        lv_CutitronicsPresID = result.ServiceOutput.CutitronicsPresID;
        lv_CutitronicsBrandID = result.ServiceOutput.CutitronicsBrandID;
        lv_PrescriptionName = result.ServiceOutput.PrescriptionName;
        lv_SkinType = result.ServiceOutput.SkinType;

        //Add product button
        var lv_ProductButton = "<a class='btn btn-outline-light' href='/add-pres-product.html?BrandID=" + lv_CutitronicsBrandID + "&PresID=" + lv_CutitronicsPresID + "'>Add Product</a> <a class='btn btn-outline-light pl-2' href='/modify-prescription.html?PresID=" + lv_CutitronicsPresID + "&PresName=" + lv_PrescriptionName + "&SkinType=" + lv_SkinType + "'>Modify Prescription Details</a>";
        $('#addProduct').html(lv_ProductButton);

        lv_ProductDiv = $('#ProductDiv');

        for(lv_Product of lv_PrescriptionProducts) {

            var lv_PoductHTML = "<div class='row pt-2'> <div class='col'> <form id='productForm" + lv_PrescriptionProducts.indexOf(lv_Product) + "'> <div class='card bg-light'> <div class='card-header'> <p class='h4' id='SKU Name'>" + lv_Product.CutitronicsSKUName 
            + "</p> <p> <span id='SKULabel" + lv_PrescriptionProducts.indexOf(lv_Product) + "' style='font-weight: bold;'>Cutitronics SKU Code: </span>" + "<span id='SKUvalue" + lv_PrescriptionProducts.indexOf(lv_Product) + "' value='" + lv_Product.CutitronicsSKUCode + "'>" + lv_Product.CutitronicsSKUCode +" </span>"
            + " </p> </div> <div class='card-body'> <p class='FrequencyDet'><span id='Frequency' style='font-weight: bold;'>Frequency: </span>  <span id='FrequencyType' style='font-weight: bold;'>Frequency Type: </span>" + lv_Product.FrequencyType 
            + "<span id='PrescriptionType' style='font-weight: bold;'> Prescription Type: </span>" + lv_Product.PrescriptionType 
            + "</p> <p class='UsageDet'><span id='Recommended Amount' style='font-weight: bold;'>Recommended Amount: </span>" + lv_Product.RecommendedAmount 
            + "<br><span id='UsageAM' style='font-weight: bold;'>UsageAM: </span>" + lv_Product.UsageAM + " <span id='UsagePM' style='font-weight: bold;'> UsagePM: </span>" + lv_Product.UsagePM 
            + "</p><p><a class='btn btn-outline-info' id='modifyProduct' href='/modify-usage.html?" + "PresID="+lv_CutitronicsPresID + "&" + "SKUCode="+lv_Product.CutitronicsSKUCode + "&" + "PresName="+lv_PrescriptionName + "&PresDetID="+lv_Product.CutitronicsPresDETID + "'>Modify Usage Details</a> <button type='submit' class='btn btn-outline-info' data-toggle='modal' data-target='#confirmDelete' id='ModalButton'>Remove From Prescription</button></p></div></div></form></div></div>";

            lv_ProductDiv.append(lv_PoductHTML);
        }

        $('#productForm0').submit(function (e) { 
            e.preventDefault();

            console.log(e);

            //Set global SKU Code
            console.log("SKUCode: " + $('#SKUvalue0').html());
            Prescription['DeleteSKU'] = $('#SKUvalue0').html();
    
        });

        $('#productForm1').submit(function (e) { 
            e.preventDefault();

            console.log(e);

            //Set global SKU Code
            console.log("SKUCode: " + $('#SKUvalue1').html());
            Prescription['DeleteSKU'] = $('#SKUvalue1').html();
    
        });

        $('#productForm2').submit(function (e) { 
            e.preventDefault();

            console.log(e);

            //Set global SKU Code
            console.log("SKUCode: " + $('#SKUvalue2').html());
            Prescription['DeleteSKU'] = $('#SKUvalue2').html();
    
        });

        $('#productForm3').submit(function (e) { 
            e.preventDefault();

            console.log(e);

            //Set global SKU Code
            console.log("SKUCode: " + $('#SKUvalue3').html());
            Prescription['DeleteSKU'] = $('#SKUvalue3').html();
    
        });

        $('#productForm4').submit(function (e) { 
            e.preventDefault();

            console.log(e);

            //Set global SKU Code
            console.log("SKUCode: " + $('#SKUvalue4').html());
            Prescription['DeleteSKU'] = $('#SKUvalue4').html();
    
        });

        $('#productForm5').submit(function (e) { 
            e.preventDefault();

            console.log(e);

            //Set global SKU Code
            console.log("SKUCode: " + $('#SKUvalue5').html());
            Prescription['DeleteSKU'] = $('#SKUvalue5').html();
    
        });

        $('#productForm6').submit(function (e) { 
            e.preventDefault();

            console.log(e);

            //Set global SKU Code
            console.log("SKUCode: " + $('#SKUvalue6').html());
            Prescription['DeleteSKU'] = $('#SKUvalue6').html();
    
        });
    }


    function deletePresItem(lv_PresItem) {
        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + '/prescriptions/deletePresProduct',
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: completeDelete,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed API Request")
                console.error('Error requesting ride: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
                alert('An error occured when requesting your unicorn:\n' + jqXHR.responseText);
            },
            data: JSON.stringify(lv_PresItem)
        });
    }

    function completeDelete(result) {
        window.location.href = "/prescription-products.html?"+Prescription.PresID+"|"+Prescription.PresName+"|"+Prescription.SkinType;
    }


    function onclickDelete(lv_String) {

    }

    $(function onDocReady() {

        var query = decodeURIComponent(window.location.search.substring(1));
        var parms = query.split('|');

        //Auto-fill param values on page load
        $('#titleDesc').html("View and modify your " + parms[1] + " prescription and it's products on this page");
        $('#PresTitle').html(parms[1] + " Products");
        $('#PresID').html(parms[0]);
        Prescription["PresID"] = parms[0];
        $('#PresName').html(parms[1]);
        Prescription["PresName"] = parms[1];
        $('#SkinType').html(parms[2]);
        Prescription["SkinType"] = parms[2];

        console.log("calling getPrescribed products");
        getPrescribedProducts(parms[0]);

        $('#DeleteProductBtn').click(function (e) { 
            
            console.log("Delete product form pres");
            console.log(Prescription.PresID[0]);
            console.log(Prescription.DeleteSKU);

            var lv_PresItem = {
                "CutitronicsPresID": Prescription.PresID,
                "CutitronicsSKUCode": Prescription.DeleteSKU
            }

            deletePresItem(lv_PresItem);
        });
    });

    
}(jQuery));