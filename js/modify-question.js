var AdminScreen = window.AdminScreen || {};
var Question = {};

(function modifyQuestionScopeWraper($) {
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in")
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });

    $(function onDocReady() {

        console.log("");
        console.log("modify-question load function running...");
        console.log("");

        //Get parameters from URL
        var query = decodeURIComponent(window.location.search.substring(1));
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });

        //Set global variables
        Question.CutitronicsBrandID = result.BrandID;
        Question.QuestionKey = result.QKey;
        Question.QuestionText = result.QText;

    })

    
    // Add Listener to back button
    $("#BackBtn").click(function (e) { 
        window.history.back();
    });

    // Add listener to create question button 
    $("#modifyQuestion").click(function (e) { 
        e.preventDefault();

        if(Question.CutitronicsBrandID != "") {

            lv_Payload = {
                "QuestionText": $('#QuestionText').val(),
                "QuestionKey": Question.QuestionKey,
                "CutitronicsBrandID": Question.CutitronicsBrandID
            }

            fn_ModifyQuestion(lv_Payload);

        }
        
    });
    

    function fn_ModifyQuestion(lv_Payload) {

        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + "/HAYGOQuestions/modifyQuestion",
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: fn_completeRequest,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed to create new question, try again");
                console.error('Response: ', jqXHR.responseText);
            },
            data: JSON.stringify(lv_Payload)
        });

    }


    function fn_completeRequest(result){

        console.log("")
        console.log("Successfully Completed execution")
        console.log("")

        var lv_QuestionText = result.ServiceOutput.QuestionText;
        window.alert("Successfully changed question text to '" + lv_QuestionText + "'");
        window.location.href = '/HAYGO-Questions.html';

    }
    
}(jQuery));