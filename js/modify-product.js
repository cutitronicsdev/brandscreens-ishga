var AdminScreen = window.AdminScreen || {};
var Product = {};


(function modifyProductScopeWraper($) {

    //Authorisation check for API request
    var authToken;
    AdminScreen.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
        } else {
            window.alert("Please sign in");
            window.location.href = '/sign-in.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/sign-in.html';
    });

    //Start of execution
    $(function onDocReady() {
        
        //Get parameters from URL
        var query = decodeURIComponent(window.location.search.substring(1));
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });

        //Change product detail placeholders
        fn_showProductDetails(result.BrandID, result.SKUCode);

        //Form has been submitted
        $('#Confirm').click(function (e) { 
            e.preventDefault();

            //Get new values from form or use existing from API.
            lv_APIInput = fn_getFormValues();

            console.log("Form has been submitted with the following input");
            console.log(lv_APIInput);

            //Make API request /updateProduct
            fn_changeProductDetails(lv_APIInput);
            
        });
    })

    //Helper Functions

    // API Call: /getProduct
    function fn_showProductDetails(lv_CutitronicsBrandID, lv_CutitronicsSKUCode) {
        $.ajax({
            type: 'GET',
            url: window._config.api.invokeUrl + "/products/getProduct",
            headers: {
                Authorization: authToken
            },
            success: fn_changePlaceholders,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                console.error('Error getting brand product: ', textStatus, ', Details: ', errorThrown);
                console.error('Response: ', jqXHR.responseText);
            },
            data: {
                CutitronicsBrandID: lv_CutitronicsBrandID,
                CutitronicsSKUCode: lv_CutitronicsSKUCode
            }
        })
    }

    function fn_changePlaceholders(result) {
        const lv_BrandID = result.ServiceOutput.CutitronicsBrandID;

        // TODO This needs to be a global to allow blank values to be entered
        const lv_ProductDetails = result.ServiceOutput.BrandProduct;
        Product = lv_ProductDetails;
        Product.CutitronicsBrandID = lv_BrandID;

        console.log("Prouct Details for SKU: " + lv_ProductDetails.CutitronicsSKUCode);
        console.log(Product);


        $('#CutitronicsSKUName').attr('placeholder', lv_ProductDetails.CutitronicsSKUName);
        $('#CutitronicsSKUDesc').attr('placeholder', lv_ProductDetails.CutitronicsSKUDesc);
        $('#CutitronicsSKUCode').attr('placeholder', lv_ProductDetails.CutitronicsSKUCode);
        $('#CutitronicsSKUCat').attr('placeholder', lv_ProductDetails.CutitronicsSKUCat);
        $('#CutitronicsSKUType').attr('placeholder', lv_ProductDetails.CutitronicsSKUType);
        $('#DefaultURL').attr('placeholder', lv_ProductDetails.DefaultURL);
        $('#DefaultIMG').attr('placeholder', lv_ProductDetails.DefaultIMG);
        $('#DefaultVideo').attr('placeholder', lv_ProductDetails.DefaultVideo);
        $('#DefaultVolume').attr('placeholder', lv_ProductDetails.DefaultVolume);
        $('#DefaultParams').attr('placeholder', lv_ProductDetails.DefaultParameters);
        $('#RecAmount').attr('placeholder', lv_ProductDetails.RecommendedAmount);
    }


    function fn_getFormValues() {

        var lv_FormValues = {}

        //Get values from form, if empty use default
        if($('#CutitronicsSKUName').val().trim().length === 0) {
            lv_FormValues.CutitronicsSKUName = Product.CutitronicsSKUName;
        } else {
            lv_FormValues.CutitronicsSKUName = $('#CutitronicsSKUName').val();
        }

        if($('#CutitronicsSKUDesc').val().trim().length === 0) {
            lv_FormValues.CutitronicsSKUDesc = Product.CutitronicsSKUDesc;
        } else {
            lv_FormValues.CutitronicsSKUDesc = $('#CutitronicsSKUDesc').val();
        }

        if($('#CutitronicsSKUCode').val().trim().length === 0) {
            lv_FormValues.CutitronicsSKUCode = Product.CutitronicsSKUCode;
        } else {
            lv_FormValues.CutitronicsSKUCode = $('#CutitronicsSKUCode').val();
        }

        if($('#CutitronicsSKUCat').val().trim().length === 0) {
            lv_FormValues.CutitronicsSKUCat = Product.CutitronicsSKUCat;
        } else {
            lv_FormValues.CutitronicsSKUCat = $('#CutitronicsSKUCat').val();
        }

        if($('#CutitronicsSKUType').val().trim().length === 0) {
            lv_FormValues.CutitronicsSKUType = Product.CutitronicsSKUType;
        } else {
            lv_FormValues.CutitronicsSKUType = $('#CutitronicsSKUType').val();
        }

        if($('#DefaultURL').val().trim().length === 0) {
            lv_FormValues.DefaultURL = Product.DefaultURL;
        } else {
            lv_FormValues.DefaultURL = $('#DefaultURL').val();
        }

        if($('#DefaultIMG').val().trim().length === 0) {
            lv_FormValues.DefaultIMG = Product.DefaultIMG;
        } else {
            lv_FormValues.DefaultIMG = $('#DefaultIMG').val();
        }

        if($('#DefaultVideo').val().trim().length === 0) {
            lv_FormValues.DefaultVideo = Product.DefaultVideo;
        } else {
            lv_FormValues.DefaultVideo = $('#DefaultVideo').val();
        }

        if($('#DefaultVolume').val().trim().length === 0) {
            lv_FormValues.DefaultVolume = Product.DefaultVolume;
        } else {
            lv_FormValues.DefaultVolume = $('#DefaultVolume').val();
        }

        if($('#RecAmount').val().trim().length === 0) {
            lv_FormValues.RecommendedAmount = Product.RecommendedAmount;
        } else {
            lv_FormValues.RecommendedAmount = $('#RecAmount').val();
        }

        if($('#DefaultParams').val().trim().length === 0) {
            lv_FormValues.DefaultParameters = Product.DefaultParameters;
        } else {
            lv_FormValues.DefaultParameters = $('#DefaultParams').val();
        }

        lv_FormValues.CutitronicsBrandID = Product.CutitronicsBrandID;

        return lv_FormValues;
    }

    
    function fn_changeProductDetails(lv_NewProduct) {
        $.ajax({
            method: 'POST',
            url: window._config.api.invokeUrl + '/products/updateBrandProduct',
            headers: {
                Authorization: authToken
            },
            dataType: "json",
            contentType: 'application/json',
            success: fn_showNewProduct,
            error: function ajaxError(jqXHR, textStatus, errorThrown) {
                window.alert("Failed API Request: Update Product");
                console.error('Error updating product: ', textStatus, ', Details: ', errorThrown);
            },
            data: JSON.stringify(lv_NewProduct)
        });
    }
    
    function fn_showNewProduct(result) {
        console.log("Successful API Call");

        console.log(JSON.stringify(result));

        window.location.href = "/view-products.html";
    }
    
}(jQuery));