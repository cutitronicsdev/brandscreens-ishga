var AdminScreen = window.AdminScreen || {};

(function scopeWrapper($) {
    var signinUrl = '/sign-in.html';

    var poolData = {
        UserpoolId: _therapist-config.cognito.usePpoolId,
        ClientId: _therapist-config.cognito.userPoolClientId
    };

    var userPool;

    if (!(_therapist-config.cognito.userPoolId && 
        _therapist-cognito.userPoolClientId &&
        _therapist-cognito.region)) {
            $('#noAccountDetails').show();
            return;
    }
     
    userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

    if (typeof AWSCognito !== 'undefined') {
        AWSCognito.config.region = _therapist-config.cognito.region;
    }

    AdminSreen.singOut = function singOut() {
        userPool.getCurrentUser().signOut();
    };

    AdminScreen.authToken = new Promise(function fetchCurrentAuthToken(resolve, reject) {
        var cognitoUser = userPool.getCurrentUser();

        if(cognitoUser) {
            cognito.getSession(function sessionCallback(err, session) {
                if(err) {
                    reject(err);
                    // logg error
                    console.error(err);
                } 
                else if (!session.isValid()) {
                    resolve(null);
                } 
                else {
                    resolve(session.getIdToken().getJwtToken());
                }
            });
        } else {
            // not a cognito user
            resolve(null);
        }
    });
    

    // Userpool function


    function register(email, password, onSuccess, onFailure) {

        var dataEmail = {
            Name: 'email',
            Value: email
        };
        var attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);

        userPool.signUp(toUsername(email), password, [attributeEmail], null,
            function signUpCallback(err, result) {
                if(!err) {
                    onSuccess(result);
                } else {
                    onFailure(err);
                    console.error(err); 
                }
            }
        
        );
    }

    function signin(email, password, onSuccess, OnFailure) {
        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
            Username: toUsername(email),
            Password: password
        });

        var cognitoUser = createCognitoUser(email);
        cognitoUser.authenticateUser(authenticationDetails,{
            onSuccess: function (result) {
                // Therapist has been successfullly authenticated
            }, //onSuccess,
            onFailure:  function (result) {
               // Therapist could not be authenticated
            },//onFailure

            newPasswordRequired: function(CognitoTherapistAttributes, requiredAttributes){
                // Therapist was signed up by Admin and must provide a new password to complete authentication

                delete CognitoTherapistAttributes.email_verified;

                sessionUserAttributes = CognitoTherapistAttributes;
            }
        });



        // change password for an authenticated user
        cognitoUser.changePassword('oldPassword', 'newPassword', function(err, result){
            if (err) {
                alert(err.message || JSON.stringify(err));
                return;
            }
            console.log('call result: ' + result);
        });
    }

    function verify(email, code, onSuccess, onFailure) {
        createCognitoUser(email).confirmRegistration(code, true, function confirmCallback(err, result) {
            if (!err) {
            onSuccess(result);
            } else {
                onFailure(err);
                console.error(err);
            }
         });

    }


    function createCognitoUser(email) {
        return new AmazonCognitoIdentity.CognitoUser({
            Username: toUsername(email),
            Pool: userPool
        });
    }

    function toUsername(email){
        return email.replace('@', '-at-');
    }

    export function resetPassword(username) {
        /* Check 
            var poolData = {
                UserpoolId: _therapist-config.cognito.userPoolId,
                ClientId: _therapist-config.cognito.userPoolId
            } */
        // userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

        // create new cognitoUser
            
        cognitoUser = createCognitoUser('email');

        // call forgotPassword on cognitoUser
        cognitoUser.forgotPassword({
            onSuccess: function(result) {
                console.log('call result: ' + result ); 
            },
            onFailure: function(err) {
                alert(err);
            },
            inputVerificationCode() {// optional
                var verificationCode = prompt('Please enter verification code ', '');
                var newPassword = prompt('Enter new password: ', '');
                cognitoUser.confirmPassword(verificationCode, newPassword, this);

            }
        });

    }

    // alternative for confirmPassword 

    export function confirmPassword(username, verificationCode, newPassword) {

        cognitoUser = creteCognitoUser('email');

        return new Promise((resolve, reject) => {
            cognitoUser.confirmPassword(verificationCode, newPassword, {
                onFailure(err) {
                    reject(err);
                    alert(err);
                },
                onSuccess() {
                    resolve();
                }
            });
        });
    }

    

    // Event handlers 

    $(function onDocReady() {
        $('#signinForm').submit(handleSignin);
        $('#registrationForm').submit(handleRegister);
        $('#verifyForm').submit(handleVerify);

    });

    function handleSignin(event) {
        var email = $('#emailInputSignin').val();
        var password = $('#passwordInputSignin').val();
        event.preventDefault();
        signin(email, password,
            function signinSuccess() {
                console.log('Successfully Logged In');
                window.location.href = 'index.html';
            },
            function signinError(err) {
                alert(err);
            }
        );

    }

    function handleRegister(event) {
        var email = $('#emailInputRegister').val();
        var password = $('#passwordInputRegister').val();
        var password2 = $('#password2InputRegister').val();

        var onSuccess = function registerSuccess(result) {
            var cognitoUser = result.user;
            console.log('user name is ' + cognitoUser.getUsernme());
        
            var confirmation = ('Registration successful. Please check your email inbox or junk mail for a verification code.');
            if(confirmation) {
                window.location.href = 'verify.html';
                }

        };
        
        var onFailure = function registerFailure(err) {
             alert(err);
         };
         event.preventDefault();

         if(password == password2) {
             register(email, password, onSuccess, onFailure);
         } else {
             alert('Passwords do not match');
         }
    }

    function handleVerify(event) {
        var email = $('#emailInputVerify').val();
        var code = $('#codeInputVerify').val();
        event.preventDefault();
        verify(email, code,     
            function verifySuccess(result) {
                console.log('call result: ' + result);
                console.log('Successfully verified');
                alert('Verification successful. You will be redirected to the login page.');
                window.location.href = signinUrl;
            },
            function verifyError(err) {
                alert(err);
            }
        );
    }




}(jQuery));

   
